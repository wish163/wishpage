<?php
/////// common ///////////////////////////////////////////////////////////
$c_instance = array();
$m_instance = array();
$v_instance = array();

function setDebug($str){
    $GLOBALS['debug'] .= $str . '<br />';
}

function C($controller)
{
    if(empty($c_instance[$controller])){
        $c_path = "./app/c/{$controller}.php";
        if(DEBUG) setDebug("controller_path: {$c_path}");
        if(!file_exists($c_path)){
            exit("The controller not found");
        }
        include $c_path;
        $c_instance[$controller] = new $controller();
    }
    return $c_instance[$controller];
}

function M($module)
{
    if(empty($m_instance[$module])){
        $m_path = "./app/m/{$module}.php";
        if(DEBUG) setDebug("module_path: {$m_path}");
        if(!file_exists($m_path)){
            exit("The module not found");
        }
        include $m_path;
        $m_instance[$module] = new $module();
    }
    return $m_instance[$module] ;
}

function V($view)
{
    if(empty($v_instance[$view])){
        $v_path = "./app/v/{$view}.php";
        if(DEBUG) setDebug("view_path: {$v_path}");
        if(!file_exists($v_path)){
            exit("The view not found");
        }
        include $v_path;
        $v_instance[$view] = new $view();
    }
    return $v_instance[$view] ;
}
///////////////// 核心类 /////////////////////////////////////////////////
class controller{
    function __call($methodName, $methodArgs){
        echo __CLASS__, "::", $methodName, " not found";
    }
}
