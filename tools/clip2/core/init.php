<?php
//屏蔽错误
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
date_default_timezone_set('Asia/Shanghai');

//定义常量
define ("ENV",  'dev');
define ("DEBUG",  true);
define ("ROOT_PATH",  dirname(dirname(__FILE__)));

//获取控制器和action
$uri = $_SERVER['REQUEST_URI'];
$uri = explode('/', $uri);
$action = array_pop($uri);
$controller = array_pop($uri);
$action = empty($action) ? 'index' : $action;
$controller = empty($controller) ? 'index' : $controller;

//加载公共库
$coms = glob(ROOT_PATH.'/common/*.php');
foreach($coms as $com){
    include($com);
}

//调用控制器函数
function run(){
    C($controller)->$action();
}

