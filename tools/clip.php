<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
date_default_timezone_set('Asia/Shanghai');
const EVN = ''; //dev relase
$URL_DOMAIN = EVN=='dev' ? 'http://localhost' : 'http://wish.oschina.mopaas.com/tools';
$path = EVN=='dev' ? "." : getenv('MOPAAS_FILESYSTEM16683_LOCAL_PATH').'/'.getenv('MOPAAS_FILESYSTEM16683_NAME');
function showTip($msg='', $go='', $delay=2000, $exit=true){
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    echo '<script>';
    echo 'parent.showTip("'.$msg.'","'.$go.'" ,'.intval($delay).');';
    echo '</script>';
    if($exit) exit;
}
function go($url){
    header('location:'.$url); exit;
}
function savaData($data_path, $harddata){
    file_put_contents($data_path, base64_encode(serialize($harddata)));
}
function getData($data_path){
    return unserialize(base64_decode(file_get_contents($data_path)));
}
function getRand($num){
    $arrs = array_merge(range('a','z'),range('A','Z'),range('0','9'));
    $coun = count($arrs);
    $randStr = '';
    for($i=0;$i<$num;$i++){
        $k = rand(0,$coun-1);
        $randStr .= $arrs[$k];
    }
}
function getFormatHtml($html){
    if(empty($html)) return '';
    $html = str_replace(PHP_EOL, '<br />', $html); // 替换换行符
    $html = preg_replace('/\[(http:\/\/.*?)\]/i', '<a href="$1" target="_blank">$1</a>', $html); //替换超级链接
    $html = preg_replace('/\[img (.*?)\]/i', '<img src="$1" />', $html); //替换图片
    $html = preg_replace('/\[down (.*?)\]/i', '附件：$1 <a href="$1">下载</a>', $html); //替换附件
    return $html;
}
$t = isset($_GET['t']) ? trim($_GET['t']) : 'send';
$u = isset($_GET['u']) ? trim($_GET['u']) : '';
$d = isset($_GET['d']) ? trim($_GET['d']) : '';
if($t=='down'){
    $conf = include('./conf.php');
    go($conf['down_url']);
}
if($t=='new' && !empty($d)){
    setcookie("u", $d, time()+31104000); //保存1年
    go('?t=help');
}
if(empty($u)){
    $u = empty($_COOKIE['u']) ? '' : $_COOKIE['u'];
    //$u = empty($_SESSION['u']) ? '' : $_SESSION['u'];
} else {
    setcookie("u", $u, time()+31104000);  //保存1年
    //$_SESSION['u'] = $u;
}
if($_POST || $t=='receive' || $t=='del'){
    $u_data = empty($u) ? 'default' : $u;
    $data_path = $path.'/clip.'.$u_data.'.dat';
    $harddata = getData($data_path);
}
if($t=='del' && $harddata){
    $id = $_GET['id'];
    if(is_numeric($id)){
        array_splice($harddata, $id, 1);
        savaData($data_path, $harddata);
        showTip('删除成功！','?t=receive', 1000);
    } else {
        showTip('删除失败！','?t=receive', 1000);
    }
}
if($_POST){
    $newdata = array(
        'data'=>$_POST['text'],
        'time'=>time(),
    );
    if($harddata){
        array_unshift($harddata, $newdata);
    } else {
        $harddata = array($newdata);
    }
    savaData($data_path, $harddata);
    showTip('发送成功！');
}
$seled = 'color:#321;background:#f80;';
$unseled = 'color:#00e;';
if($t=='send'){
    $asend = $seled;
    $areceive = $unseled;
} else {
    $asend = $unseled;
    $areceive = $seled;
    $bdline = 'border-bottom:1px solid #A9A9A9;';
}
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>我的云剪切板</title>
    <style type="text/css">
        body{padding:0;margin:0;}
        .nav{font-size:12px;width:300px;position: fixed;padding:8px 8px 2px;background:#fff;}
        .nav a{font-size:12px;padding:2px;width:30px;}
        .selbtn{float:right;}
        .datalist ul{padding:8px;margin:8px;}
        .datalist li{line-height: 26px;word-wrap:break-word;}
        .time{color:#999;font-size:12px;}
        .del a{color:#999;font-size:12px;}
        .content{padding-top:28px;padding-left:8px;}
        #submitFrame{display:none;height:0;width:0;}
        #tip{display: none;position:absolute;width: 200px;height: 40px;background: orange;color: black;left:50%;top:50%;margin-left: -100px;margin-top: -20px;font-size: 16px;font-weight: bold;text-align: center;line-height: 40px;}
        #clipform{margin:0;}
        .help{padding:0 10px;line-height: 200%;}
        .help .help_content{padding-top: 10px;}
        .help h3{margin-bottom: 0;}
        .help hr{margin: 0;}
        .use_skill{line-height: 160%;}
        .sbmit_btn a{font-size: 12px;}
    </style>
</head>
<body>
    <?php if($t=='help'){ ?>
        <div class="help">
            <h3>我的云剪切板帮助文档</h3>
            <hr>
            <div class="help_content">
            1. 添加右侧链接到收藏栏（<strong>可拖动到收藏栏</strong>）
            <?php if(empty($_COOKIE['u'])){?>
            <a href="javascript:var user=prompt('请输入用户名','');if(user)location.replace('?t=new&d='+user);">请先点击这里生成用户名</a><br />
            <?php } else {?>
            <a href="javascript:d=document;void(wc=window.open('<?php echo $URL_DOMAIN;?>/clip.php?t=send&u=<?php echo $_COOKIE['u']?>','_blank', 'location=no,links=no,scrollbars=no,toolbar=no,width=370,height=404,left=405,top=120,status=no,resizable=no'));wc.focus();">我的云剪切板</a><br />
            <?php }?>
            2. 点击右侧链接下载客户端 <a href="?t=down" target="_blank">下载我的云剪切板</a><br />
            3. 使用技巧：<br />
            <div class="use_skill">
                <div>&nbsp;&nbsp;&nbsp;<strong>[修改用户名]</strong></div>
                <div>&nbsp;&nbsp;&nbsp;如果客户端想修改用户名，请在文件浏览器中输入“%appdata%”并回车，进入appdata目录，查找appdata目录下WishSoft/default.table文件，用记事本打开，并修改其中的用户名即可。</div>
                <div>&nbsp;&nbsp;&nbsp;<strong>[多媒体支持]</strong></div>
                 <div>&nbsp;&nbsp;&nbsp;1) 超链接支持：[http://doamin.com] 生成 &lt;a href="http://domain.com">http://domain.com&lt;/a&gt;</div>
                 <div>&nbsp;&nbsp;&nbsp;2) 图片支持：[img http://domain.com/xx.jpg] 生成 &lt;img src="http://domain.com/xx.jpg" /&gt;</div>
                 <div>&nbsp;&nbsp;&nbsp;3) 附件支持：[down http://domain.com/xx.rar] 生成 附件：http://domain.com/xx.rar&lt;a href="http://domain.com/xx.rar">下载&lt;/a&gt;</div>
            </div>
            <div>
        </div>
    <?php } else { ?>
        <div class="nav" style="<?php echo $bdline;?>">
            <span class="selbtn">
                <a href="?t=send" style="<?php echo $asend;?>;">发送</a>&nbsp;
                <a href="?t=receive" style="<?php echo $areceive;?>;">接收</a>
                <a href="?t=help" target="_blank">?</a>
            </span>
            <span>我的云剪切板 <?php echo ($u!=''&&$u!='default')?'('.$u.')':'';?></span>
        </div>
        <div class="content">
            <?php if($t=='send'){ ?>
            <form action="?" method="post" target="submitFrame" id="clipform">
                <div><textarea name="text" style="width:300px;height:200px;"></textarea></div>
                <div class="sbmit_btn"><input type="submit" value=" 发 送 " />&nbsp;&nbsp;<a href="http://www.mftp.info" target="_blank">分享图片</a>&nbsp;<a href="http://www.1000eb.com" target="_blank">分享文件</a></div>
            </form>
            <?php } else { ?>
            <div class="datalist">
                <ul>
                    <?php foreach($harddata as $k=>$v){ ?>
                    <li>
                        <span class="data"><?php echo getFormatHtml($v['data']);?></span>
                        <?php echo strpos(trim($v['data']), PHP_EOL)!==false?'<br />':'' ?>
                        <span class="time"><?php echo date('Y.m.d',$v['time']);?></span>
                        <span class="del"><a href="?t=del&id=<?php echo $k;?>" target="submitFrame">删除</a></span>
                    </li>
                    <?php } ?>
                </ul>
            </div>
            <?php } ?>
        </div>
        <div id="tip"></div>
        <iframe name="submitFrame" id="submitFrame" src="about:blank"></iframe>
        <script>
            function showTip(str, gourl, delaytime){
                if(typeof(delaytime) == 'undefined') delaytime = 2000;
                if(typeof(gourl) == 'undefined') gourl = '';
                var tip = document.getElementById('tip');
                tip.style.display = 'block';
                tip.innerHTML = str;
                setTimeout(function(){
                    tip.style.display = 'none';
                    if(gourl!='') location.replace(gourl);
                }, delaytime);
            }
        </script>
    <?php } ?>
</body>
</html>