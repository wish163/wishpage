<?php
include "common.php";
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>我的云剪切板帮助文档</title>
    <style type="text/css">
        body{padding:0;margin:0;font-size: 12px;}
        a{color: bule;}
        a:link{color: bule;}
        a:visited{color: blue;} 
        a:hover{color: red;}
        .help{padding:0 10px;line-height: 200%;}
        .help .help_content{padding-top: 10px;}
        .help h3{margin-bottom: 0;}
        .help hr{margin: 0;}
        .use_skill{line-height: 160%;}
    </style>
</head>
<body>
    <div class="help">
        <h3>我的云剪切板帮助文档</h3>
        <hr>
        <div class="help_content">
        1. 添加右侧链接到收藏栏（<strong>可拖动到收藏栏</strong>）
        <?php if(empty($_COOKIE['u'])){?>
        <a href="javascript:var user=prompt('请输入用户名','');if(user)location.replace('post.php?t=new&d='+user);">请先点击这里生成用户名</a><br />
        <?php } else {?>
        <a href="javascript:d=document;void(wc=window.open('<?php echo $URL_DOMAIN;?>/index.php?t=send&u=<?php echo $_COOKIE['u']?>','_blank', 'location=no,links=no,scrollbars=no,toolbar=no,width=370,height=404,left=405,top=120,status=no,resizable=no'));wc.focus();">我的云剪切板</a><br />
        <?php }?>
        2. 点击右侧链接下载客户端 <a href="post.php?t=client" target="_blank">下载我的云剪切板</a><br />
        3. 使用技巧：<br />
        <div class="use_skill">
            <div>&nbsp;&nbsp;&nbsp;<strong>[修改用户名]</strong></div>
            <div>&nbsp;&nbsp;&nbsp;客户端，如果想修改用户名，请在文件浏览器中输入“%appdata%”并回车，进入appdata目录，查找appdata目录下WishSoft/default.table文件，用记事本打开，并修改其中的用户名即可。</div>
            <div>&nbsp;&nbsp;&nbsp;浏览器收藏栏版，如果想修改用户名，请在收藏栏链接中修改相应的用户名或修改cookie。</div>
            <div>&nbsp;&nbsp;&nbsp;<strong>[多媒体支持]</strong></div>
             <div>&nbsp;&nbsp;&nbsp;1) 超链接支持：[http://doamin.com] 生成 &lt;a href="http://domain.com">http://domain.com&lt;/a&gt;</div>
             <div>&nbsp;&nbsp;&nbsp;2) 图片支持：[img http://domain.com/xx.jpg] 生成 &lt;img src="http://domain.com/xx.jpg" /&gt;</div>
             <div>&nbsp;&nbsp;&nbsp;3) 附件支持：[down http://domain.com/xx.rar] 生成 附件：http://domain.com/xx.rar&lt;a href="http://domain.com/xx.rar">下载&lt;/a&gt;</div>
        </div>
        <div>
    </div>
</body>
</html>