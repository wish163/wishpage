<?php
include 'route.php';
include "common.php";
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>我的云剪切板</title>
    <style type="text/css">
        body{padding:0;margin:0;font-size: 12px;overflow:hidden;padding: 0 2px;}
        a{color: bule;}
        a:link{color: bule;}
        a:visited{color: blue;} 
        a:hover{color: red;}
        #clipform{margin:0;}
        .text{width: 100%;height:60px;border: 1px solid #ccc;outline: none;font-size: 12px;margin-left: -3px;overflow: auto;}
        .send_file{position: relative;}
        .send_file a{font-size: 12px;}
        .send_btn {float: right;}
        .list{margin-top: 2px;width: 100%;border: 0;}
        .upload{position: absolute;left: 0;top:-26px;height: 21px;width: 300px;border: 0;display: none;}
        a.menubtn{color: #999; text-decoration: none;position: relative;top:2px;}
        .submenu{position: relative;background: #fff;}
        .submenu ul{display: none;background: #fff;border: 1px solid #ccc;width: 60px;position: absolute;top:12px;left: 0;}
        #menubox{margin:0;padding: 0;}
        #menubox li{list-style: none;}
        #menubox li a{padding: 4px;display: inline-block;width: 52px;text-decoration: none;}
        #menubox li a:hover{background: #eee;}
        #tip{display: none;position:absolute;width: 200px;height: 40px;background: orange;color: black;left:50%;top:50%;margin-left: -100px;margin-top: -20px;font-size: 16px;font-weight: bold;text-align: center;line-height: 40px;}
        #submitFrame{display:none;height:0;width:0;}
        .more a{color: #999;}
    </style>
</head>
<body scroll="no">
<form action="?act=post" method="post" target="submitFrame" id="clipform">
    <div><textarea name="text" class="text" hidefocus="true"></textarea></div>
    <div class="send_file">
        <span class="send_btn">
            <?php echo (isset($u) && $u!=''&&$u!='default')?'('.$u.')':'';?>
            <input type="submit" value="发送" title="ctrl+enter发送">
        </span>
        <a href="javascript:showUpload();" title="如果是图片可预览，文件大小不能超过50M">发送文件</a>
        <span class="more">
            <a href="javascript:showShot();">截图</a>
            <a href="javascript:freshList();" class="submenu">刷新列表</a>
            <a href="?act=help" target="_blank">帮助</a>
        </span>
        <span class="submenu" onmouseout="hideMenu()">
            <a href="javascript:showMenu()" class="menubtn">▼</a>
            <ul id="menubox" onmouseover="showMenu()" onmouseout="hideMenu()">
                <li><a href="<?php echo $conf['share_image'];?>" target="_blank">分享图片</a></li>
                <li><a href="<?php echo $conf['share_file'];?>" target="_blank">分享文件</a></li>
                <li><a href="<?php echo $conf['cloud_note'];?>" target="_blank">云笔记</a></li>
                <li><a href="<?php echo $conf['cloud_disk'];?>" target="_blank">云网盘</a></li>
            </ul>
        </span>
        <iframe class="upload" id="upload" src="?act=upload"></iframe>
    </div>
</form>
<iframe class="list" id="list" src="?act=list"></iframe>
<iframe name="submitFrame" id="submitFrame" src="about:blank"></iframe>
<div id="tip"></div>
<script type="text/javascript">
    function showShot(){
        if(typeof(external.doShot)!='undefined'){
            external.doShot();
        } else {
            showTip("目前只有客户端支持截图");
        }
    }
    function showUpload(){
        document.getElementById('upload').style.display = 'block';
    }
    function cancelUpload(){
       document.getElementById('upload').style.display = 'none'; 
    }
    function resetUpload(){
        var oupload = document.getElementById('upload');
        oupload.src = oupload.src;
    }
    function showMenu(){
        document.getElementById('menubox').style.display = 'block';
    }
    function hideMenu(){
        document.getElementById('menubox').style.display = 'none';
    }
    function showTip(str, gourl, delaytime, style){
        if(typeof(delaytime) == 'undefined') delaytime = 2000;
        if(typeof(gourl) == 'undefined') gourl = '';
        if(typeof(style) == 'undefined') style = '';
        var tip = document.getElementById('tip');
        tip.style.display = 'block';
        tip.innerHTML = "<span style='"+style+"'>"+str+"</span>";
        if(delaytime>-1){
            setTimeout(function(){
                tip.style.display = 'none';
                if(gourl!='') document.getElementById('list').src = gourl;
            }, delaytime);
        }
    }
    function freshList(){
        var olist = document.getElementById('list');
        olist.src = olist.src;
    }
    function caclFrameHeight(){
        document.getElementById('list').style.height = (document.documentElement.clientHeight-95)+'px';
    }
    caclFrameHeight();
    window.onresize = function(){
        caclFrameHeight();
    }
    //ctrl+enter键提交
    window.document.onkeydown = function(e){
        e = window.event || e;
        if(event.ctrlKey && e.keyCode==13){
            document.getElementById('clipform').submit();
        }
    }
</script>
</body>
</html>