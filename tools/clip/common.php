<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
date_default_timezone_set('Asia/Shanghai');
const EVN = ''; //dev relase
$URL_DOMAIN = EVN=='dev' ? 'http://wishpage/tools/clip' : 'http://wish.oschina.mopaas.com/tools/clip';
$path = EVN=='dev' ? dirname(__FILE__) : getenv('MOPAAS_FILESYSTEM16683_LOCAL_PATH').'/'.getenv('MOPAAS_FILESYSTEM16683_NAME');
$conf = include('./conf.php');

//获取参数
$t = isset($_REQUEST['t']) ? trim($_REQUEST['t']) : 'send';
$u = isset($_REQUEST['u']) ? trim($_REQUEST['u']) : '';
$d = isset($_REQUEST['d']) ? trim($_REQUEST['d']) : '';
$client = isset($_REQUEST['client']) ? trim($_REQUEST['client']) : '';

//获取用户cookie
if(empty($u)){
    $u = empty($_COOKIE['u']) ? '' : $_COOKIE['u'];
} else {
    setcookie('u', $u, time()+31104000);  //保存1年
}

//判断是否为客户端
if(empty($client)){
    $client = empty($_COOKIE['client']) ? '' : $_COOKIE['client'];
} else {
    setcookie('client', $client, time()+31104000);  //保存1年
}

//获取数据
if($_POST || $t=='list' || $t=='del' || $t=='image' || $t=='down'){
    global $harddata,$data_path;
    $u_data = empty($u) ? 'default' : $u;
    $data_path = $path.'/data/clip.'.$u_data.'.dat';
    $harddata = getData($data_path);
    $GLOBALS['data_path'] = $data_path;
    $GLOBALS['harddata'] = $harddata;
}

function showTip($msg='', $go='', $delay=2000, $exit=true, $parent='parent'){
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    echo '<script>';
    echo $parent.'.showTip("'.$msg.'","'.$go.'" ,'.intval($delay).');';
    echo '</script>';
    if($exit) exit;
}

function go($url){
    header('location:'.$url); exit;
}

function savaData($data_path, $harddata){
    global $path;
    make_dir($path.'/data');
    //file_put_contents("test.txt", print_r($harddata,true));
    file_put_contents($data_path, base64_encode(serialize($harddata)));
}

function getData($data_path){
    return unserialize(base64_decode(file_get_contents($data_path)));
}

function getRand($num){
    $arrs = array_merge(range('a','z'),range('A','Z'),range('0','9'));
    $coun = count($arrs);
    $randStr = '';
    for($i=0;$i<$num;$i++){
        $k = rand(0,$coun-1);
        $randStr .= $arrs[$k];
    }
    return $randStr;
}

function getFormatHtml($data, $k){
    if(empty($data) || !is_numeric($k)) return '';
    $html = $data['data'];
    if(isset($data['type']) && $data['type']==2){
        $title = !empty($data['title']) ? $data['title'] : $html;
        $html = '<a href="'.'?act=post&t=image&id='.$k.'" target="_blank"><img width="30" height="30" border="0" align="absmiddle" onload="AutoResizeImage(100,30,this)"  src="'.'?act=post&t=image&id='.$k.'" /></a> '.$title.' <a href="'.'?act=post&t=down&id='.$k.'" target="listSubmitFrame">下载</a>';
    } else if(isset($data['type']) && $data['type']==3){
        $title = !empty($data['title']) ? $data['title'] : $html;
        $html = '<strong>附件：</strong>'.$title.' <a href="'.'?act=post&t=down&id='.$k.'" target="listSubmitFrame">下载</a>';
    } else {
        $html = str_replace(PHP_EOL, '<br />', $html); // 替换换行符
        $html = preg_replace('/\[(http:\/\/.*?)\]/i', '<a href="$1" target="_blank">$1</a>', $html); //替换超级链接
        $html = preg_replace('/\[img (.*?)\]/i', '<a href="$1" target="_blank"><img width="30" height="30" border="0" align="absmiddle" onload="AutoResizeImage(100,30,this)"  src="$1" /></a> $1 <a href="$1" target="listSubmitFrame">下载</a>', $html); //替换图片
        $html = preg_replace('/\[down (.*?)\]/i', '<strong>附件：</strong>$1 <a href="$1" target="listSubmitFrame">下载</a>', $html); //替换附件
    }
    return $html;
}

function make_dir($path){
    if(!is_dir($path)){
        umask(0000);
        mkdir($path, 0777, true);
    }
}
