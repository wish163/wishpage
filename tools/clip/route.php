<?php
$act = isset($_REQUEST['act']) ? $_REQUEST['act'] : '';
switch($act)
{
    case 'help':
        direct('help.php');
        break;
    case 'list':
        $_REQUEST['t'] = 'list';
        direct('list.php');
        break;
    case 'upload':
        direct('upload.php');
        break;
    case 'post':
        direct('post.php');
        break;
}
function direct($file_path){
    include $file_path;
    exit;
}