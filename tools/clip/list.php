<?php
include "common.php";
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>list</title>
    <style type="text/css">
        body{padding:0;margin:0;font-size: 12px;}
        .list{margin: 0 auto;}
        .list a{color: blue}
        ul{border-bottom: 1px solid #ccc;margin: 0;padding: 4px 0;}
        li{list-style: none;padding: 0;line-height: 180%;}
        .date{color: #ccc;}
        .date a{float: right;color: #999;}
        a:hover{color: red;}
        .cont{word-wrap:break-word;}
        #listSubmitFrame{display:none;height:0;width:0;}
    </style>
    <script type="text/javascript">
        function AutoResizeImage(maxWidth, maxHeight, objImg) {
            var img = new Image();
            img.src = objImg.src;
            var hRatio;
            var wRatio;
            var Ratio = 1;
            var w = img.width;
            var h = img.height;
            wRatio = maxWidth / w;
            hRatio = maxHeight / h;
            if (maxWidth == 0 && maxHeight == 0) {
                Ratio = 1;
            } else if (maxWidth == 0) { //
                if (hRatio < 1) Ratio = hRatio;
            } else if (maxHeight == 0) {
                if (wRatio < 1) Ratio = wRatio;
            } else if (wRatio < 1 || hRatio < 1) {
                Ratio = (wRatio <= hRatio ? wRatio: hRatio);
            }
            if (Ratio < 1) {
                w = w * Ratio;
                h = h * Ratio;
            }
            objImg.height = h;
            objImg.width = w;
            objImg.style.display = 'inline';
        }
    </script>
</head>
<body>
<div class="list">
    <?php $count=count($harddata);foreach($harddata as $k=>$v){ ?>
    <ul style="<?php echo ($count==($k+1)) ? 'border-bottom:0;' : '';?>">
        <li class="cont"><?php echo getFormatHtml($v, $k);?></li>
        <li class="date"><a href="?act=post&t=del&id=<?php echo $k;?>" target="submitFrame">删除</a><?php echo date('Y.m.d H:i:s',$v['time']);?></li>
    </ul>
    <?php } ?>
</div>
<iframe name="listSubmitFrame" id="listSubmitFrame" src="about:blank"></iframe>
</body>
</html>