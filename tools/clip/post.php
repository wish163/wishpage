<?php
include "common.php";

//显示图片
if($t=='image'){
    global $harddata;
    $id = isset($_REQUEST['id']) ? intval($_REQUEST['id']) : 0;
    $fileInfo = $harddata[$id];
    $file_path = $path."/upload/".$u;
    $file_path = $file_path . '/'. $fileInfo['data'];
    header("Content-type: image/jpeg");
    echo getFile($file_path);
    exit;
}

//下载文件
if($t=='down'){
    global $harddata;
    $id = isset($_REQUEST['id']) ? intval($_REQUEST['id']) : 0;
    $fileInfo = $harddata[$id];
    $file_path = $path."/upload/".$u;
    $file_path = $file_path . '/'. $fileInfo['data'];
    $file_ext = getExt($fileInfo['data']);
    $file_name = !empty($fileInfo['title']) ? $fileInfo['title'] : $fileInfo['data'];
    if (!file_exists($file_path)){
        showTip('下载失败！文件不存在！', '', 2000, true, 'parent.parent');
    }
    header("Content-type: text/html; charset=utf-8");
    header("Content-type: application/octet-stream");
    header("Accept-Ranges: bytes");
    header("Accept-Length: ".filesize($file_path));
    header("Content-Disposition: attachment; filename=". iconv("UTF-8","GBK",$file_name) );
    echo getFile($file_path);
    exit;
}

//上传文件
if($t=='upload'){
    if ($_FILES["file"]["error"] > 0){
         reponseErr("文件上传失败，请重试！");
    }
    if ($_FILES["file"]["size"] > 52428800){ //50M
         reponseErr("文件超过50M，请重试！");
    }
    if($_FILES["file"]["type"] == "image/gif"
        || $_FILES["file"]["type"] == "image/jpeg"
        || $_FILES["file"]["type"] == "image/pjpeg"
        || $_FILES["file"]["type"] == "image/bmp"
    )
    {
        $type = 2; //图片
    } else {
        $type = 3; //附件
    }

    //重命名文件
    $upload_path = $path."/upload/".$u;
    make_dir($upload_path);
    $file_name =  $_FILES["file"]["name"];
    if (file_exists($upload_path.'/'.$file_name))
    {
        //生成新的文件名
        $sExtArr = explode('.', $_FILES["file"]["name"]);
        $sExt = end($sExtArr);
        $file_name = $sExtArr[0].'_'.getRand(4).'.'.$sExt;
    }
    move_uploaded_file($_FILES["file"]["tmp_name"], $upload_path.'/'. $file_name);
    
    savePost(array("text"=>urlencode($file_name),"title"=>$file_name), $type);
    
    reponseOK('上传成功！');
}

//下载客户端
if($t=='client'){
    $conf = include('./conf.php');
    go($conf['down_client']);
}

//创建用户cookie
if($t=='new' && !empty($d)){
    setcookie('u', $d, time()+31104000); //保存1年
    go('help.php');
}

//删除数据
if($t=='del' && $harddata){
    $id = $_GET['id'];
    if(is_numeric($id)){
        array_splice($harddata, $id, 1);
        savaData($data_path, $harddata);
        showTip('删除成功！','?act=list&t=list', 1000);
    } else {
        showTip('删除失败！','?act=list&t=list', 1000);
    }
}

//提交数据
if($_POST){
    if(empty($_POST['text'])){
        showTip('不能发送空内容！');
    }
    savePost($_POST);
    showTip('发送成功！','?act=list&t=list');
}

function savePost($post, $type = 1){
    $data_path = $GLOBALS['data_path'];
    $harddata = $GLOBALS['harddata'];
    $newdata = array(
        'title'=> isset($post['title']) ? $post['title'] : '',
        'data'=> $post['text'],
        'time'=> time(),
        'type'=> $type
    );
    if($harddata){
        array_unshift($harddata, $newdata);
    } else {
        $harddata = array($newdata);
    }
    savaData($data_path, $harddata);
}

function reponseOK($str, $type=0){
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    if($type==0){
        echo '<script>';
        echo 'parent.resetUpload();parent.cancelUpload();parent.showTip("上传成功！");parent.freshList();';
        echo '</script>';
    } else {
        echo '<style>body{margin:0;padding:0;font-size:12px;}a{color:blue;}</style>';
        echo $str, '&nbsp;';
        echo '<a href="javascript:history.back();">返回</a>&nbsp;';
        echo '<a href="javascript:parent.resetUpload();parent.cancelUpload();">关闭</a>';
    }
    exit;
}
function reponseErr($str, $type=1){
    reponseOK($str, $type);
}

function getFile($file_path){
    $fp= fopen($file_path,"r");
    $buffer_size = 2048;
    $cur_pos = 0;
    $filesize = sprintf("%u", filesize($file_path));
    while(!feof($fp)&&$filesize-$cur_pos>$buffer_size)
    {
        $buffer = fread($fp,$buffer_size);
        echo $buffer;
        $cur_pos += $buffer_size;
    }
    $buffer = fread($fp,$filesize-$cur_pos);
    fclose($fp);
    return $buffer;
}

function getExt($fileName){
    if(empty($fileName)) return '';
    $sExtArr = explode('.', $fileName);
    if(empty($sExtArr)) return '';
    return end($sExtArr);
}