<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>我的剪切板后台管理</title>
    <style type="text/css">
        body{padding:0;margin:0;font-size: 12px;}
        a{color: bule;}
        a:link{color: bule;}
        a:visited{color: blue;} 
        a:hover{color: red;}
        dl{line-height: 180%;}
        .submitFrame{width: 0;height: 0;display: none;}
        .menulist{float: left;width: 200px;padding:0 8px;}
        .main{float: left;border-left: 1px solid #ccc;}
        .top{height: 30px;line-height: 30px;background: #eee;padding:0 8px;}
        .login{float: right;}
        .mainFrame{border: 0;}
    </style>
</head>
<body>
<div class="top">
    <div class="login">
        admin
        <a href="">退出</a>
    </div>
    <strong>我的剪切板后台管理</strong>
</div>
<div class="menulist">
    <dl>
        <dt>文件管理</dt>
        <dd><a href="">查看所有文件</a></dd>
        <dd><a href="">清理20天前的文件</a></dd>
        <dd><a href="">清理失效的文件</a></dd>
        <dd><a href="">全部清理</a></dd>
    </dl>
</div>
<div class="main">
    <iframe src="about:blank" name="main" class="mainFrame" id="mainFrame"></iframe>
</div>
<iframe src="about:blank" name="submitFrame" class="submitFrame"></iframe>
<script type="text/javascript">
    function caclFrameHeight(){
        var mainFrame = document.getElementById('mainFrame');
        mainFrame.style.width = (document.documentElement.clientWidth-240)+'px';
        mainFrame.style.height = (document.documentElement.clientHeight-40)+'px';
    }
    caclFrameHeight();
    window.onresize = function(){
        caclFrameHeight();
    }
</script>
</body>
</html>