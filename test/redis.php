<?php

//使用autoload加载相关库,这边重点就是为了require $file;
spl_autoload_register(function($class) {
    $file = __DIR__.'/lib/Predis/'.$class.'.php';
    if (file_exists($file)) {
        require $file;
        return true;
    }
});
//配置连接的IP、端口、以及相应的数据库
$server = array(
    'host'     => '10.4.7.20',
    'port'     => 5200,
    'database' => 15
    );
    $redis = new Client($server);
    //普通set/get操作
    $redis->set('library', 'predis');
    $retval = $redis->get('library');
    echo $retval; //显示 'predis'